//
//    Define Histogram Classes Hist1d and Hist2d
//
#ifndef HIST_H
#define HIST_H
#include <iostream>
#include <math.h>
#include <string>

class Axis {
friend class Hist1d;
friend class Hist2d;
public:
    Axis(const int nBins, const double min, const double max, const std::string &label = (std::string) "x");
    ~Axis() {/*delete[] label;*/}
    void reset() {below = above = 0.0; smallest = HUGE_VAL; biggest = -HUGE_VAL;}
    int bin(const double xval) const;
    int nbins() const {return nBins;}
private:
    std::string label;
    int nBins;
    double min;
    double max;
    double binsize;
    double below;
    double above; 
    double smallest;
    double biggest;
};

//
//   I want to be able to have arrays of ptrs to hists, which can be 1d or 2d or dummy
//   So I make a base-class Hist that satisfies all these, and is a dummy. You should 
//   create one dummy hist and make all array els point to it; then overwrite when
//   a 1d or 2d is booked.
//

class Hist {
public:
    Hist(const std::string &histTitle = " ");
    ~Hist() {/*delete[] title;*/ if (data) delete[] data;}
    virtual void fill(const double xval, const double weight = 1.) {}
    virtual void fill(const double x, const double y, const double w) {} // Cannot make default w = 1. ... clashes with 1D
    virtual int save(const std::string &filename) const {return 0;}
    virtual void reset() {}
    virtual int bin(const double xval, const double yval = 0.0) const {return -1;}
    virtual double content(const double xval, const double yval = 0.0) const {return 0.;}
    virtual double content(const int bin) const {return -1.;}
    virtual void set(int bin, double val) {};
    virtual Axis xAxis() {return Axis(1, 0., 1., "x");}
    virtual Axis yAxis() {return Axis(1, 0., 1., "y");}
protected:
    std::string title;
    double *data;
};

class Hist1d: public Hist {
public:
    Hist1d(const int nx, const double xmin, const double xmax,
           const std::string &title = (std::string) " ", 
           const std::string &axisLabel = (std::string) "x");
    void fill(const double xval, const double weight = 1.0);
    int save(const std::string &filename) const;
    void reset();
    int bin(const double xval, const double yval = 0.0) const {return x.bin(xval);}
    double content(const double xval, const double yval = 0.0) const {return content(bin(xval));}
    double content(const int bin) const {if (bin >= 0 && bin < x.nBins) return data[bin]; 
                                         else {std::cerr << "Bin out of range!\n"; return -1;}}
    void set(const int bin, const double val) {if (bin >= 0 && bin < x.nBins) data[bin] = val; 
                                               else std::cerr << "Bin out of range!\n"; 
                                               return;}
    Axis xAxis() {return x;}
    Axis yAxis() {std::cerr << "Error: requst for y-axis in 1D\n"; return Axis(1, 0., 1., "y");}
private:
    Axis x;
};

class Hist2d: public Hist {
public:
    Hist2d(const int nx, const double xmin, const double xmax,
           const int ny, const double ymin, const double ymax,
           const std::string &title = " ", 
           const std::string &xlab = "x", const std::string &ylab = "y");
    void fill(const double x, const double y) {std::cerr << 
        "Error in Hist2D fill(x, y): must give weight\n";}
    void fill(const double x, const double y, const double weight);
    int save(const std::string &filename) const;
    void reset();
    int bin(const double xval, const double yval) const;
    double content(const double xval, const double yval) const {return content(bin(xval, yval));}
    double content(const int bin) const {if (bin >= 0 && bin < x.nBins * y.nBins) return data[bin]; 
                                         else {std::cerr << "Bin out of range!\n"; return -1;}}
    void set(const int bin, const double val) {if (bin >= 0 && bin < x.nBins * y.nBins) data[bin] = val; 
                                               else std::cerr << "Bin out of range!\n";
                                               return;} 
    Axis xAxis() {return x;}
    Axis yAxis() {return y;}
private:
    Axis x;
    Axis y;
};

#endif //HIST_H
