//
//   Read in cpphist histogram definitions from an ASCII file.
//   uses freein C++ routines.
//   Derived from c code (Hessey ca. 1990) derived from FORTRAN code (hessey ca. 1988)
//
#include <fstream>
#include <iostream> // for debug
#include "FreeIn.h"
#include "Hist.h"
#include "HistDir.h"

#include <string.h>
#include <stdlib.h>
#define TRUE 1
#define FALSE 0

#define MAXCHAR  30
#define MAXTITLE 80
#define MAXCHOPT 5
#define MAXDIR   1000

int HistDir::bookHists(const std::string &filename, std::ostream *echo1, std::ostream *echo2) {
    char word[MAXCHAR], title[MAXTITLE], fullTitle[2*MAXTITLE], 
     chopt[MAXCHOPT], style[MAXCHAR];
    FreeInFile fileIn;
    int errors = 0;
    int id, nx, ny, maxID;
    double xmin, xmax, ymin, ymax;
    int n1, n2, noffset;
    char *hashPos;
    char *next, *temp, *end;
    int length, nStart, nStop;
    char curdir[MAXDIR] = "";
//
//----------------------------------------------------------------------------
//
//
//   Open definition file for FreeIn input.
//
    std::ifstream ipstream(filename.c_str()); // Open the file
    if (!ipstream.is_open()) {
        std::cerr << "bookHists: Unable to open " << filename << " for histogram definitions\n";
        return ++errors;
    }
//
//    Should not be a directory, but I don't know how to check for that???
//

    fileIn.open(ipstream);
    fileIn.echoStreams(echo1, echo2);
    fileIn.lower(FALSE);
//
//   Loop over main verbs (create, cd, mkdir, ...). Stop at EOF or exit.
//
    while (strlen(fileIn.word(word, MAXCHAR)) != 0) {
        if (strncmp(word, "exit", 4) == 0)
            break;
        else if (strncmp(word, "create", 5) == 0) {
           if (strlen(curdir) == 0) {
               fileIn.error((char *) "bookHists: must create a directory first");
               return ++errors;
           }
//
//   Create histograms. First see if 1D, 2D, or profile.
//
            fileIn.word(word, MAXCHAR);
            if (strncmp(word, "1d", 2) == 0) {
//
//   Book 1D histograms. Stop at non-positive id.
//
                while ((id = (int) fileIn.number()) > 0) {
                    fileIn.string(title, MAXTITLE);
                    nx = (int) fileIn.number();
                    xmin = fileIn.number();
                    xmax = fileIn.number();
                    if (id < this->operator[](curdir).size) {
                        this->operator[](curdir).hist[id] = new Hist1d(nx, xmin, xmax, title);
                    }
                    else {
                        fileIn.error((char *)"bookHists: id too big for directory");
                        ++errors;
                    }
                }
            }
            else if (strncmp(word, "2d", 2) == 0) {
//
//   Book 2D histograms. Stop at non-positive id.
//
                 while ((id = (int) fileIn.number()) > 0) {
                    fileIn.string(title, MAXTITLE);
                    nx = (int) fileIn.number();
                    xmin = fileIn.number();
                    xmax = fileIn.number();
                    ny = (int) fileIn.number();
                    ymin = fileIn.number();
                    ymax = fileIn.number();
                    if (id < this->operator[](curdir).size) {
                        this->operator[](curdir).hist[id] = 
                           new Hist2d(nx, xmin, xmax, ny, ymin, ymax, title);
                    }
                    else {
                        fileIn.error((char *) "bookHists: id too big for directory");
                        ++errors;
                    }
                }
            }
/*   profile-hist not implemented in Hist yet
            else if (strncmp(word, "profile", 4) == 0) {
//
//   Book profile histograms. Stop at non-positive id.
//
                while ((id = (int) fileIn.number()) > 0) {
                    fileIn.string(title, MAXTITLE);
                    nx = (int) fileIn.number();
                    xmin = fileIn.number();
                    xmax = fileIn.number();
                    ymin = fileIn.number();
                    ymax = fileIn.number();
                    fileIn.word(chopt, MAXCHOPT);
                    hbprof_(&id, title, &nx, &xmin, &xmax, 
                              &ymin, &ymax, chopt, strlen(title), strlen(chopt));
                }
            }
*/
            else {
                fprintf(stderr, "bookHists: Unknown histogram type %s\n",
                   word);
                fileIn.error((char *) "Expecting 1d, 2d, or profile");
                errors++;
            }
        }
        else if (strncmp(word, "mkdir", 5) == 0) {
//
//    Create a new histogram directory; AND cd to it.
//
            fileIn.word(curdir, MAXDIR);
            maxID = (int) fileIn.number();
            this->operator[](curdir).setSize(maxID);
        }
        else if (strncmp(word, "cd", 2) == 0) {
//
//    Change to a new subdirectory.
//
            fileIn.word(curdir, MAXDIR);
        }
        else if (strncmp(word, "spec", 4) == 0) {
//
//    Make a long run of hists with same parameters, different titles and id's
//
            fileIn.word(style, MAXCHAR);
            fileIn.string(title, MAXTITLE);
            if ((hashPos = strchr(title, '#')) == NULL)
                strncpy(fullTitle, title, 2*MAXTITLE);
            else {
                *hashPos = '\0';
            }
            n1 = (int) fileIn.number();
            n2 = (int) fileIn.number();
            noffset = (int) fileIn.number();
            nx = (int) fileIn.number();
            xmin = fileIn.number();
            xmax = fileIn.number();
            if (strncmp(style, "2d", 2) == 0) {
                ny = (int) fileIn.number();
                ymin = fileIn.number();
                ymax = fileIn.number();
                for (id = n1 + noffset; id <= n2 + noffset; id++) {
                    if (hashPos)
                        sprintf(fullTitle, "%s%d%s", 
                         title, id - noffset, hashPos + 1);
                    if (id < this->operator[](curdir).size) {
                        this->operator[](curdir).hist[id] = 
                           new Hist2d(nx, xmin, xmax, ny, ymin, ymax, fullTitle);
                    }
                    else {
                        fileIn.error((char *) "bookHists: id too big for directory");
                        ++errors;
                    }
                }
            }
/* profile not yet implemented
            else if (strncmp(style, "prof", 4) == 0) {
                ymin = fileIn.number();
                ymax = fileIn.number();
                fileIn.word(chopt, MAXCHOPT);
                for (id = n1 + noffset; id <= n2 + noffset; id++) {
                   if (hashPos)
                      sprintf(fullTitle, "%s%d%s", 
                       title, id - noffset, hashPos + 1);
                   hbprof_(&id, fullTitle, &nx, &xmin, &xmax, 
                             &ymin, &ymax, chopt, strlen(fullTitle), strlen(chopt));
                }
            }
*/
            else if (strncmp(style, "1d", 2) == 0) {
                for (id = n1 + noffset; id <= n2 + noffset; id++) {
                    if (hashPos)
                       sprintf(fullTitle, "%s%d%s", 
                        title, id - noffset, hashPos + 1);
                    if (id < this->operator[](curdir).size) {
                        this->operator[](curdir).hist[id] = 
                           new Hist1d(nx, xmin, xmax, fullTitle);
// std::cout << id << " " << nx << fullTitle << std::endl;
                    }
                    else {
                        fileIn.error((char *) "bookHists: id too big for directory");
                        ++errors;
                    }
                }
            }
            else {
                fprintf(stderr, 
                 "bookHists: unknown hist type %s in definition file\n", word);
                fileIn.error((char *) "Expecting 1d, 2d, or prof");
                errors++;
            }
        }
        else if (strncmp(word, "del", 3) == 0) {
//
//    Delete some histograms. Rest of line is a list in form 1 2 3 10 - 20 30
//    etc.
//
            fileIn.string(title, MAXTITLE);
//
//    Process string to get ranges of hist id's to zero
//
            length = strlen(title);
            temp = next = title;
            end = next + length;
            while (next < end) {
                nStart = strtol(next, &temp, 0);
                if (nStart == 0 && temp == next)
                    break; /* No conversion signifies end of string, or bad string */
                next = temp;
                while (*next == ' ') /* Skip to next non-space char */
                    next++;
                if (*next == '-')
                    nStop = strtol(++next, &next, 0);
                else
                    nStop = nStart;
                for (id = nStart; id <= nStop; id++)
                    std::cerr << "bookHists: Histogram deletion not yet implemented";
/*
                    if (id == 0 || hexist_(&id))
                        hdelet_(&id);
*/
             }
         }
         else {
             fprintf(stderr, "bookHists: unknown control word %s in definition file\n", 
                word);
             fileIn.error((char *) "Expecting create, cd, mkdir, special, or exit");
             errors++;
         }
     }
     return errors;
}
