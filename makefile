OBJECTS = bookhists.o Hist.o HistDir.o 
# Assumes you have lib and include directories in your HOME directory
# Main product is libhist.a. "tryit" is just a small program to try it out,
# and giving example of how to use it.

CPPFLAGS = -c -g -I.. -I$(HOME)/include 

tryit: libhist.a test.o
	g++ -g -o tryit test.o -L . -lhist -L $(HOME)/lib -lfreein

libhist.a: $(OBJECTS)
	ar -r libhist.a $(OBJECTS) 
	cp libhist.a $(HOME)/lib
	cp Hist.h HistDir.h $(HOME)/include

$(OBJECTS): Hist.h HistDir.h

test.o: Hist.h HistDir.h
