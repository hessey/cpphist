//
//   HistDir member functions
//
#include "HistDir.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <string>
#include <sstream>

using namespace std;

// For the histogram pointers to be initialised to:
Hist dummyHist("Dummy Hist");

void HistList::setSize(int n, Hist *dummyHist) {
//
//   Really only intended for use once. If someone resizes, all old list
//   is deleted (avoid memory leak). But you could copy them to the new list,
//   allowing expansion.
//
    if (size > 0) delete[] hist;
    size = n;
    hist = new Hist *[n];
    while (n) hist[--n] = dummyHist;
}
int HistList::save(const string &dirName) const {

char id[10];
std::string fullname;
    for (int i = 1; i < size; i++) {
        sprintf(id, "/%06d", i);
        fullname = dirName + id; 
        hist[i]->save(fullname);  
    }
    return 0;
}

void HistDir::addDir(std::string &name, int n, Hist *dummy) {
    this->operator[](name).setSize(n);
}

int HistDir::save(const string &rootName) const {
    if (mkdir(rootName.c_str(), 0777) && errno != EEXIST) {
        std::cerr << "HistDir::save - Unable to create directory " << rootName << '\n';
        return errno;
    }

    int rc = 0;
    std::string fullname;
    for (std::map <std::string, HistList>::const_iterator i = begin(); 
           i != end() && !rc; ++i) {
        fullname = rootName + "/" + i->first;
        if (mkdir(fullname.c_str(), 0777) && errno != EEXIST) {
            std::cerr << "HistDir::save - Unable to create directory " << fullname << '\n';
            return errno;
        }
        rc = i->second.save(fullname);
    }
    return rc;
}
