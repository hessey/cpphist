cppHist is a simple histogramming package. Written in C++, it provides header files, a lib-file, and documentation on how to use it.

It allows creation of 1D and 2D histograms, filling them with weighted values, and saving the resulting histograms to disk (in an ascii format). These can be read in in R (or define your own ROOT macros to display in ROOT).

Installation: 
    Download the files
    Edit the makefile to suit your username and where you want the files to be stored
    make
Use:
    Read the Manual.txt file, and look at the example file tryit.cpp to get going.
    Look in the sub-directory R to get an idea of how to display histograms in R.

Nigel Hessey. Gitlab added January 2024. (Originally cppHistString).
