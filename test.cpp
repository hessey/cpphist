//
//    Test Hist routines
//
#include <stdlib.h>
#include <stdio.h>
#include "Hist.h"
#include "HistDir.h"

int main () {
Hist1d oned(100, 0.0, 100.0, "Silly hist", "The x-axis");
int i;

    for (i = 0; i < 100; ++i) {
        oned.fill((double) i + 0.5, (double) i*i);
    }
    oned.save("1dhist");
    oned.reset();
    oned.save("1dreset");
Hist2d twod(20, 0.0, 100.0, 25, 0.0, 150, "Two-d Hist", "x-axis (mm)", "y-axis (mm)");
    for (int i = 0; i < 100; i++) {
        for (int j = 0; j < 150; j++) {
            twod.fill(i + 0.5, j + 0.5, i * j);
        }
    }
    twod.save("2dhist");
    twod.reset();
    twod.save("2dreset");

HistDir histTree;
histTree.bookHists("test.hst");

std::cout << "Fill bookHist hists...\n";
for (int i = -10; i < 10; ++i) {
    histTree["crystals"].hist[1]->fill(i, i*i);
std::cout << "fill with i = " << i << std::endl;
}

std::cout << "Save them...\n";
histTree.save();

std::cout << "...and exit\n";
exit(0);
}
