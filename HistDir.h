//
//   Make a directory structure to hold arrays of histogram pointers
//   Two purposes: 
//        (i) allow use of hist id's, so histogram definitions can be read 
//        from an ascii file
//        (ii) so a whole tree of hists can be saved
//   The array is initialised with ptrs to a dummy hist, so that a call to fill
//   an undefined histogram is not an error and simply returns.
//   This basically allows an hbook type use of hists. The hist package can be used
//   without any of this stuff.
//
#ifndef HISTDIR_H
#define HISTDIR_H

#include <stdio.h> // Temporary!
#include <map>
#include <string>
#include "Hist.h"

extern Hist dummyHist;

//
//   A list of histograms...
//
class HistList {
public:
    int size;
    Hist **hist;
    void setSize(int n, Hist *dummy = &dummyHist);
    int save(const std::string &directoryName) const;
};

//
//    A directory: it has histograms and subdirectories. The name is a character string,
//    used as the key for the map.
//
class HistDir: public std::map <std::string, HistList> {
public:
    void addDir(std::string &name, int n, Hist *dummy = &dummyHist);
    int save(const std::string &rootName = "./histograms/") const;
    int bookHists(const std::string &filename, std::ostream *echo1 = &std::cout, 
                                         std::ostream *echo2 = 0); 
};

#endif //HISTDIR_H
