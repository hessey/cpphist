//
//   Methods for Hist class
//
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <string>
#include <math.h>
#include "Hist.h"

using namespace std;

Axis::Axis(const int numBins, const double lowEdge, const double highEdge, 
           const string &axisLabel) {

    if (numBins < 1 || lowEdge > highEdge) {
        std::cerr << "Bad number of bins or extremes given to Axis constructor \n";
        exit(1);
    }
    nBins = numBins;
    min = lowEdge;
    max = highEdge;
    binsize = (max - min) / nBins;
    label = axisLabel;

    reset();
}

int Axis::bin(const double val) const {
    if (val < min || val >= max)
        return -1;
    return (int) ((val - min) / binsize);
} 

Hist::Hist(const string &histTitle) {

    title = histTitle;
    data = 0;
}

Hist1d::Hist1d(const int nx, const double xmin, const double xmax,
               const string &title, const string &axlab) 
       : Hist::Hist(title), x(nx, xmin, xmax, axlab) {

    data = new double[nx];
    this->reset();
}

void Hist1d::fill(const double xval, const double w) {
//
//   First, track biggest and smallest values
//
    if (xval < x.smallest) 
        x.smallest = xval;
    if (xval > x.biggest)
        x.biggest = xval;
//
//    Now see if xv is in range; if so, fill it
//
    if (xval < x.min)
        x.below += w;
    else if (xval >= x.max)
        x.above += w;
    else { 
        data[bin(xval)] += w;
    }
}

int Hist1d::save(const string &name) const {
//
//    Save histogram to a file, suitable for other programs (e.g. R) to read
//    Actually two files are created: one with the axis definition, one with the contents
//
FILE *faxes, *fdata;
int i, j, index;

    if (!(faxes = fopen((name + ".axes").c_str(), "w"))) {
        std::cerr << "Hist1d::save... Unable to open file " << name + ".axes" << "; hist not saved\n";
        return 1;
    }

    if (!(fdata = fopen((name + ".data").c_str(), "w"))) {
        std::cerr << "Hist21::save... Unable to open file " << name + ".data" << "; hist not saved\n";
        return 2;
    }

    fprintf(faxes, "title nx xmin xmax xlab xsmallest xbiggest nxbelow nxabove ny\n");
    fprintf(faxes, " \"%s\" %d %g %g \"%s\" %g %g %g %g 0\n", 
     title.c_str(), x.nBins, x.min, x.max, x.label.c_str(), x.smallest, x.biggest, 
     x.below, x.above);

    for (i = 0; i < x.nBins; ++i) {
        fprintf(fdata, "%g\n", data[i]);
    }

    fclose(faxes);
    fclose(fdata);

    return 0;
}

void Hist1d::reset() {
    memset(data, 0, x.nBins * sizeof (data[0]));
    x.reset();
}

Hist2d::Hist2d(const int nx, const double xmin, const double xmax,
               const int ny, const double ymin, const double ymax,
               const string &title, 
               const string &xlab, const string &ylab) 
       : Hist::Hist(title), x(nx, xmin, xmax, xlab), y(ny, ymin, ymax, ylab) {

    data = new double[nx * ny];
    this->reset();
}

void Hist2d::fill(const double xval, const double yval, const double w) {
//
//   First, track biggest and smallest values
//
    if (xval < x.smallest) 
        x.smallest = xval;
    if (xval > x.biggest)
        x.biggest = xval;
    if (yval < y.smallest) 
        y.smallest = yval;
    if (yval > y.biggest)
        y.biggest = yval;
//
//    Now see if xv and yv are in range; if so, fill it
//
    if (xval < x.min)
        x.below += w;
    else if (xval >= x.max)
        x.above += w;
    else {  // It's in x-range
        if (yval < y.min)
            y.below += w;
        else if (yval >= y.max)
            y.above += w;
        else {
            data[bin(xval, yval)] += w;
            if (bin(xval, yval) < -1) std::cerr << "Oh, oh bin < 0 in 2D hist filling!\n";
        }
    }
}

int Hist2d::save(const string &name) const {
//
//    Save histogram to a file, suitable for other programs (e.g. R) to read
//    Actually two files are created: one with the axis definition, one with the contents
//
FILE *faxes, *fdata;
int i, j, index;

    if (!(faxes = fopen((name + ".axes").c_str(), "w"))) {
        std::cerr << "Hist2d::save... Unable to open file " << name + ".axes" << 
         "; hist not saved\n";
        return 1;
    }
    if (!(fdata = fopen((name + ".data").c_str(), "w"))) {
        std::cerr << "Hist2d::save... Unable to open file " << name + ".data" << 
         "; hist not saved\n";
        return 2;
    }

    fprintf(faxes, "title nx xmin xmax xlab xsmallest xbiggest nxbelow nxabove ");
    fprintf(faxes, "ny ymin ymax ylab ysmallest ybiggest nybelow nyabove \n");
    fprintf(faxes, " \"%s\" %d %f %f \"%s\" %f %f %f %f %d %f %f \"%s\" %f %f %f %f\n", 
     title.c_str(), x.nBins, x.min, x.max, x.label.c_str(), x.smallest, x.biggest, 
     x.below, x.above, y.nBins, y.min, y.max, y.label.c_str(), y.smallest, y.biggest, 
     y.below, y.above);

    index = 0;
    for (i = 0; i < x.nBins; ++i) {
        for (j = 0; j < y.nBins; ++j) {
            fprintf(fdata, "%g ", data[index++]);
            if (index % 8 == 0)
                fprintf(fdata, "\n");
        }
    } 

    fclose(faxes);
    fclose(fdata);
    return 0;
}

void Hist2d::reset() {
    memset(data, 0, x.nBins * y.nBins * sizeof (data[0]));
    x.reset();
    y.reset();
}

int Hist2d::bin(const double xval, const double yval) const {
    int tempx = x.bin(xval);
    if (tempx < 0) return tempx;
    int tempy = y.bin(yval);
    if (tempy < 0) return tempy;
    return tempx + x.nBins * tempy;
}
